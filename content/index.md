---
date: 2000-01-01T19:50:00+02:00
title: index
type: index
---

# Welcome


<div style="float: left;margin: 0px 80px 50px 0px">
    <img src="/img/profile.jpg" width="213" height="213"/>
</div>

Hi! My name is Bruno Rodrigues, and I'm a research assistant at [STATEC](http://www.statistiques.public.lu/en/actors/statec/organisation/red/index.html).

I program mostly in R and love sharing my knowledge, that's why I started this blog. 
I share my posts also on [R-bloggers](http://www.r-bloggers.com). In my posts, I discuss 
new packages I discovered or new ways of using packages. 

If you were one of my students, and need some of the materials I taught, just drop me an [email](mailto:contact@brodrigues.co).

You can follow me on [twitter](https://twitter.com/brodriguesco) for blog updates.

I'm writing a book as a hobby about functional programming, unit testing and package
development with R. You can read it for [free](https://www.brodrigues.co/fput).

You can also download my package, called [brotools](https://bitbucket.org/b-rodrigues/brotools),
which contains some functions I use daily. You might find them useful too!

# Blog posts
